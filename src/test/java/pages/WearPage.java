package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;

public class WearPage extends BasePage{

    @FindBy(css = ".js-store-prod-name.js-product-name.t-store__prod-popup__name.t-typography__title.t-name.t-name_xl")
    WebElement nameWear;

    @FindBy(css = ".js-product-price.js-store-prod-price-val.t-store__prod-popup__price-value")
    WebElement priceWear;

    public WearPage() {
        PageFactory.initElements(driver, this);
    }

    public WearPage assertNameWear(String expectedNameWear) throws InterruptedException {
        sleep(1000);
        assertEquals(expectedNameWear, nameWear.getText());
        return this;
    }

    public WearPage assertPriceWear(String expectedPriceWear) {
        assertEquals(String.valueOf(expectedPriceWear), priceWear.getAttribute("data-product-price-def-str"));
        return this;
    }
}
