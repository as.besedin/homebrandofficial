package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WearsPage extends BasePage {

    @FindBy(css = ".t-store__filter__input.js-store-filter-search")
    private WebElement searchInput;

    @FindBy(css = ".t-store__search-icon.js-store-filter-search-btn")
    private WebElement findButton;

    @FindBy(xpath = "//*[@class=\"t951__cont-wrapper\"]//*[@class=\"js-store-prod-name js-product-name t-store__card__title t-typography__title t-name t-name_md\"]")
    private List<WebElement> namesCards;

    @FindBy(xpath = "//*[@class=\"t951__cont-wrapper\"]//*[@class=\"js-product-price js-store-prod-price-val t-store__card__price-value\"]")
    private List<WebElement> pricesCards;

    @FindBy(css = ".t-store__empty-part-msg")
    private WebElement notFoundText;

    public WearsPage() {
        driver.get(config.baseUrl() + "/wear");
        PageFactory.initElements(driver, this);
    }

    public WearsPage inputSearch(String searchText) {
        searchInput.sendKeys(searchText);
        return this;
    }

    public WearsPage clickFindButton() {
        findButton.click();
        return this;
    }

    public WearsPage assertCardSize(int expectedCardSize) throws InterruptedException {
        sleep(1000);
        assertEquals("Количество найденных товаров не совпадает", expectedCardSize, namesCards.size());
        return this;
    }

    public WearsPage assertFirstNameCardText(String expectedNameWear) {
        assertEquals("Название товара не соответствует найденному", expectedNameWear, namesCards.get(0).getText());
        return this;
    }

    public WearsPage assertFirstPriceCardText(int expectedPrice) {
        assertEquals("Название товара не соответствует найденному", String.valueOf(expectedPrice), pricesCards.get(0).getAttribute("data-product-price-def-str"));
        return this;
    }

    public WearsPage assertNotFoundTextExist() throws InterruptedException {
        sleep(1000);
        assertTrue("Текст 'ничего не найдено' не отображается", notFoundText.isDisplayed());
        return this;
    }

    public WearsPage clickCard(int numberOfCard) {
        namesCards.get(numberOfCard).click();
        return this;
    }

    public String getPriceCard(int numberOfCard) {
        return pricesCards.get(numberOfCard).getAttribute("data-product-price-def-str");
    }

    public String getNameCard(int numberOfCard) {
        return namesCards.get(numberOfCard).getText();
    }
}
