import org.junit.Test;
import pages.WearPage;
import pages.WearsPage;

public class WearTest extends BaseTest {

    @Test
    public void testWearPage() throws InterruptedException {
        int numberOfCard = 4;

        WearsPage wearsPage = new WearsPage();

        String expectedNameCard = wearsPage.getNameCard(numberOfCard);
        String expectedPriceCard = wearsPage.getPriceCard(numberOfCard);

        wearsPage.clickCard(numberOfCard);

        WearPage wearPage = new WearPage();

        wearPage
                .assertNameWear(expectedNameCard)
                .assertPriceWear(expectedPriceCard);
    }
}
