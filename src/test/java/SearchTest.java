import org.junit.Test;
import pages.WearsPage;

public class SearchTest extends BaseTest {

    @Test
    public void testFindCurrentThing() throws InterruptedException {
        String nameWear = "Костюм б/н василек унисекс";
        int expectedSearchResult = 1;
        int expectedPrice = 9700;

        WearsPage wearsPage = new WearsPage();

        wearsPage
                .inputSearch(nameWear)
                .clickFindButton()
                .assertCardSize(expectedSearchResult)
                .assertFirstNameCardText(nameWear)
                .assertFirstPriceCardText(expectedPrice);
    }

    @Test
    public void testIncorrectSearch() throws InterruptedException {
        String incorrectText = "fdshvfkjvbdfksvf";

        WearsPage wearsPage = new WearsPage();
        wearsPage
                .inputSearch(incorrectText)
                .clickFindButton()
                .assertNotFoundTextExist();
    }
}
